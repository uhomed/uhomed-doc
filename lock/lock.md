# 门锁接口

1、前言

​	1.1、为了更好的提供给公寓方服务（自营公寓、公寓平台），我们将整合门锁厂商，打包提供给公寓合作伙伴更快速、便捷、安全的通讯接口，由于厂商系统权限、各种限制，打包接口如下。

2、公共参数

​	2.1、[apiKey获取地址](http://uhomed.com/user/main.html)

​	2.2、[地区数据](http://github.com/shadowsay/common-data)

​	3.3、私钥获取请联系对接人

​	3.4、[通知配置](http://uhomed.com/user/conf/notification.html)





3、接口集合

​	2.1、[添加门锁](#添加门锁)

​	2.2、[解除绑定](#解除绑定)

​	2.3、[添加密码](#添加密码)

​	2.4、[门锁列表](#门锁列表)

​	2.5、[门锁密码列表](#门锁密码列表)

​	2.6、[门锁删除密码](#门锁删除密码)

​	2.7、[远程开锁](#远程开锁)

​	2.8、[查询离线密码](#查询离线密码)

​	2.8、[密码过期通知](#密码过期通知)

​	2.9、[打开门锁通知](#打开门锁通知)

## 添加门锁

功能说明：

​	首次将门锁添加到当前帐号下，绑定房间。

绑定流程：

![绑定流程](images/add.png)

对应接口：``` https://api.uhomed.com/lock/binding.html``` *请求方式：GET*

使用说明：该页面是html5构成，可直接在应用中调用，*调用时记得打开js权限，将apiKey放入request.header中，例：apiKey=OWIJDQOWDJIQO19283DWOQJD*

接口参数：

| 参数名         | 类型     | 是否必传 | 描述                                       | 长度限制 | 示例   |
| ----------- | ------ | ---- | ---------------------------------------- | ---- | ---- |
| homeId      | String | 是    | 作为绑定唯一标识，可以理解为订单号，添加成功后直接callback参数带回    | 100  | /    |
| callbackUrl | String | 是    | 回调地址，绑定成功后返回门锁id                         | 200  | /    |
| extra       | String | 否    | 回调时扩展参数                                  | 500  | /    |
| cityId      | String | 是    | 地区ID [数据参见](https://github.com/shadowsay/common-data) | 6    | /    |
| mobile      | String | 是    | 联系人手机号                                   | 11   | /    |
| address     | String | 是    | 门锁安装地址                                   | 100  | /    |



回调结果：

```json
{
  "lockId": "f350587ca0001c8bf4654a285d04bffce347a24f41099d703c65dc81c030007d", //加密后的门锁id
  "extra":"a=1&b=2",
  "homeId":"9823417",
  "code": "000000",   //成功code
  "message": "添加门锁成功！",//描述信息
  "status": "successful" //失败时为fail
}
```



## 解除绑定

功能说明：接触门锁与云控帐号绑定。



对应接口：``` https://api.uhomed.com/lock/del.json``` *请求方式：POST*



接口参数：

| 参数名    | 类型     | 是否必传 | 描述     | 长度限制 | 示例   |
| ------ | ------ | ---- | ------ | ---- | ---- |
| lockId | String | 是    | 云控门锁编号 | 32   | /    |



返回结果：

```json
{
  "code": "000000",   //成功code
  "message": "添加门锁成功！",//描述信息
  "status": "successful" //失败时为fail
}
```



## 添加密码

功能说明：给当前门锁添加一个新密码



对应接口：``` https://api.uhomed.com/lock/passwd/add.json``` *请求类型：POST*



接口参数：

| 参数名            | 类型     | 是否必传 | 描述                      | 长度限制 | 示例                          |
| -------------- | ------ | ---- | ----------------------- | ---- | --------------------------- |
| bizId          | String | Y    | 业务id，平台自定义id，回调时返回，要求唯一 | 32   | /                           |
| lockId         | String | Y    | 门锁id                    | 32   | /                           |
| passwd         | String | N    | 门锁密码，如不传则自动生成           | 20   | /                           |
| mobile         | String | Y    | 使用人手机号                  | 11   | /                           |
| validTimeStart | Long   | Y    | 生效时间起，13位毫秒时间戳          | 13   | /                           |
| validTimeEnd   | Long   | Y    | 生效时间止，13位毫秒时间戳          | 13   | /                           |
| useName        | String | N    | 使用者姓名                   | 30   | /                           |
| cardId         | String | Y    | 使用者身份证                  | 18   | /                           |
| callbackUrl    | String | N    | 回调地址，此参数为空时则不通知密码生效结果   | 300  | http://xxx.xxx.com/xxx.json |
| extra          | String | N    | 回调自定义参数                 | 500  | a=b&c=d                     |



同步返回参数：

```json
{
  "code": "000000",   //成功code
  "message": "添加密码成功",//具体密码生效以通知结果为准。
  "status": "successful", //失败时为fail
  "lockPasswdId":"1610091059570000000981595491" //密码id
}
```



**异步结果通知描述：**

​	每当设置成功（已同步至门锁终端）系统则会发出通知，通知频率为，成功时，1，3，5，10，当收到状态为```status=successful```时止，或通知次数大于5次。

结果通知参数：

| 参数名          | 类型     | 是否必传 | 描述                   | 长度限制 | 示例   |
| ------------ | ------ | ---- | -------------------- | ---- | ---- |
| bizId        | String | Y    | 请求时平台方生成参数           | 32   | /    |
| lockPasswdId | String | Y    | 密码id                 | 32   | /    |
| status       | String | Y    | 结果描述 successful,fail |      |      |
| extra参数      | String | N    | 传入extra自定义参数         |      |      |





## 门锁列表

  ``` URL：https://api.uhomed.com/lock/list.json ```  *请求方式：POST*

  参数
| 参数名      | 类型      | 是否必传 | 描述     | 长度限制 | 示例   |
| -------- | ------- | ---- | ------ | ---- | ---- |
| currPage | Integer | Y    | 页码（数字） |      |      |

  返回数据
```json
{
  "code": "000000",
  "count": 1,
  "data": [
    {
      "address": "西溪花园", //门锁安装地址
      "cityCH": "中国,浙江省,杭州市,西湖区", //门锁安装地区
      "cityId": "330106",//安装地区id
      "lockCategory": "209210928332", //门锁类别
      "lockCategoryCH": "火河（果加）", //门锁类别描述
      "lockId": "2092109283320616100900003391", //门锁id
      "lockStatus": "COMU_ABNORMAL",// NORMAL:正常，ABNORMAL:未知异常，COMU_ABNORMAL:通讯异常，RSSI_ABNORMAL:网关异常，NODE_COMU_ABNORMAL:网关通讯异常
      "lockStatusCH": "网关通讯异常",// 状态描述
      "mobile": "18358105381",
      "power": 83, //电量，取值0-100
      "remark": "",//备注
      "syncTime": 1475984702000 //同步时间
    }
  ],
  "message": "查询成功！",
  "status": "successful"
}
```



  **注意：**
  1、火河认证方式：添加门锁后，系统需要过段时间进行数据同步，认证时间大概为半天。。。（我们尽快会协调解决认证时间问题）



## 门锁密码列表
  ``` URL：https://api.uhomed.com/lock/passwd/list.json ``` *请求方式：POST*


参数：

| 参数名    | 类型     | 是否必传 | 描述   | 长度限制 | 示例   |
| ------ | ------ | ---- | ---- | ---- | ---- |
| lockId | String | Y    | 门锁编号 | 32   |      |

  返回数据
```json
{
  "code": "000000",
  "data": [
    {
      "category": "ADMIN", //密码类别
      "categoryCH": "管理员密码", //密码类别中文
      "id": "1610091059570000000978366891",//密码id
      "passwd": "8a6f3e25a0fa42fc",//加密后的密码
      "status": "NORMAL",//密码状态
      "statusCH": "正常",//状态中文
      "useName": "fangzi",//使用者姓名
      "useMobile": "18910415779",//使用者手机号
      "cardId": "321309409283112938",//使用者身份证号
      "validEnd": 2403578090000, //密码生效止
      "validStart": 1456892690000 //密码生效起
    },
    {
      "category": "CUSTOMIZE",
      "categoryCH": "定制密码",
      "id": "1610091059570000000979816791",
      "passwd": "523aa3dab4532e97",
      "status": "NORMAL",
      "statusCH": "正常",
      "validEnd": 1506155270000,
      "validStart": 1474619270000
    },
    {
      "category": "CUSTOMIZE",
      "categoryCH": "定制密码",
      "id": "1610091059570000000980465891",
      "passwd": "3d3942f9a0d2c515a11585bdb8c21c46",
      "status": "NORMAL",
      "statusCH": "正常",
      "useMobile": "13500977924",
      "validEnd": 1478919798000,
      "validStart": 1473649398000
    },
    {
      "category": "CUSTOMIZE",
      "categoryCH": "定制密码",
      "id": "1610091059570000000981595491",
      "passwd": "cb716616a642d4d333ae3dbe0ae67116",
      "status": "NORMAL",
      "statusCH": "正常",
      "useMobile": "13500977926",
      "validEnd": 1478919798000,
      "validStart": 1473649398000
    },
    {
      "category": "CUSTOMIZE",
      "categoryCH": "定制密码",
      "id": "1610091059570000000982373491",
      "passwd": "1ec9694518aae1e4",
      "status": "NORMAL",
      "statusCH": "正常",
      "useMobile": "13460755397",
      "validEnd": 1478919798000,
      "validStart": 1473649398000
    },
    {
      "category": "CUSTOMIZE",
      "categoryCH": "定制密码",
      "id": "1610091059570000000983468391",
      "passwd": "30c8a0c7712b84e433ae3dbe0ae67116",
      "status": "NORMAL",
      "statusCH": "正常",
      "useMobile": "13500977918",
      "validEnd": 1478919798000,
      "validStart": 1473649398000
    }
  ],
  "message": "查询成功！",
  "status": "successful"
}
```

## 门锁删除密码
  ``` URL：https://api.uhomed.com/lock/passwd/del.json ``` *请求方式：POST*


参数：

| 参数名          | 类型     | 是否必传 | 描述                | 长度限制 | 示例      |
| ------------ | ------ | ---- | ----------------- | ---- | ------- |
| bizId        | String | Y    | 业务id，唯一           | 32   |         |
| lockPasswdId | String | Y    | 密码id（**门锁列表中返回**） |      |         |
| lockId       | String | Y    | 门锁编号              |      |         |
| callbackUrl  | String | N    | 回调地址              | 200  |         |
| extra        | String | N    | 自定义参数             | 500  | a=1&b=2 |

同步返回数据：
```json
  {
      "status": "successful", //失败时为fail
      "message": "删除成功！",
      "code": "000000"
  }
```

**异步结果通知描述：**

每当删除成功（**已同步至门锁终端**）系统则会发出通知，通知频率为，成功时，1，3，5，10，当收到状态为```status=successful```时止，或通知次数大于5次。

参数：

| 参数名          | 类型     | 是否必传 | 描述             | 长度限制 | 示例   |
| ------------ | ------ | ---- | -------------- | ---- | ---- |
| bizId        | String | Y    | 业务id，调用时传入原样返回 | 32   | /    |
| lockPasswdId | String | Y    | 门锁密码id         | 32   | /    |
| lockId       | String | Y    | 门锁id           | 32   | /    |
| extra参数      | String | N    | 将作为参数带回        |      |      |






## 远程开锁

  ``` URL：https://api.uhomed.com/lock/open.json ``` *请求方式：POST*

  参数：
|  参数名   |  类型  | 是否必传 |   描述   | 长度限制 |  示例  |
| :----: | :--: | :--: | :----: | :--: | :--: |
| lockId | 字符串  |  是   |  门锁编号  |  20  |  /   |
| mobile | 字符串  |  是   | 操作人手机号 |  11  |  /   |

  返回数据
```json
  {
      "status": "successful", //失败时为fail
      "message": "开门成功！",
      "code": "000000"
  }
```


## 查询离线密码

使用场景：

​	当门锁不在线时，请求离线密码可以打开门锁。

```URL：https://api.uhomed.com/lock/passwd/dynamic.json``` *请求方式：POST*

参数：

| 参数名    | 类型     | 是否必传 | 描述   | 长度限制 | 示例   |
| ------ | ------ | ---- | ---- | ---- | ---- |
| lockId | String | Y    | 门锁编号 | 32   | /    |



返回数据：

```json
{
  "code": "000000",
  "data": {
    "category": "SHORT_TERM", 
    "lockId": "2092109283320616100900003391",
    "passwd": "b878ff917a02310c33ae3dbe0ae67116",
    "status": "NORMAL",
    "useName": "2016-10-09日时效密码",
    "validEnd": 1476028799999,
    "validStart": 1475942400000
  },
  "message": "查询成功",
  "status": "successful"
}
```



## 密码过期通知

使用场景：

​	密码绑定至用户，密码过期时进行通知平台方，将密码根据自身的业务进行操作，回调方式为POST，不设置将不产生回调。[设置通知地址设置](http://www.uhomed.com/user/conf/notification.html)



参数：

| 参数名          | 类型     | 是否必传 | 描述     | 长度限制 | 示例   |
| ------------ | ------ | ---- | ------ | ---- | ---- |
| lockId       | String | Y    | 门锁id   | 32   |      |
| lockPasswdId | String | Y    | 门锁密码id | 32   |      |
| expireTime   | Long   | Y    | 13位时间戳 | 13   |      |



## 打开门锁通知

使用场景：

​	门锁被打开，不包含接口开锁，打开后将通知平台方。[设置通知地址设置](http://www.uhomed.com/user/conf/notification.html)



参数：

| 参数名                | 类型     | 是否必传 | 描述         | 长度   | 示例   |
| ------------------ | ------ | ---- | ---------- | ---- | ---- |
| lockId             | String | Y    | 门锁编号       | 32   |      |
| openTime           | Long   | Y    | 开门时间13位时间戳 | 13   |      |
| openLockCategory   | String | Y    | 开门类别       |      |      |
| openLockCategoryCH | String | Y    | 开门类别中文     |      |      |
| lockPasswdId       | String | Y    | 开门密码       |      |      |