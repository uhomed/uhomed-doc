# 接口集合



## 目的

​	优+云控将打造智能家居各行业整合统一API，服务于智能家居所需要的技术支撑，目前正在整合门锁

​	



## 目前支撑

支持的门锁厂商：

​	[果加门锁（火河）](http://www.huohetech.com/)			**已支持**

​	[云柚门锁](http://www.yeeuu.com/) 				**正在开发**

​	[科技侠](http://www.sciener.cn/)					正在开发

​	[丁盯门锁](http://www.dding.net/)					准备接入



支持的水电表厂商：

​	[上海大华智能水电表](#)			正在开发



​	



## API开放平台

平台入口：

​	[基础整合门锁接口接入](https://doc.uhomed.com/lock/lock.html)

厂商入口：

​	[绑定接入方式说明](https://doc.uhomed.com/merchant/lock/partner.html)





## 技术支持

技术支持QQ群：

​	**498793737**



技术支持电话：

​	**18500977919**



产品负责人电话：

​	**李明：18358105381**